<?php


function createfromweb_operator_attributevalues_name() {
	return "attribute: values from text";
}

function createfromweb_operator_attributevalues_execute($text) {
	$lines = explode("\n", $text);
	#print_r($lines);
	
	$result = array();
	foreach($lines as $line) {
		$keyvalue = explode(':', $line, 2); // url: http://will.get/correct
		$key = trim($keyvalue[0]);
		$value = trim($keyvalue[1]);
		$result[$key] = $value;
	}
	#print_r($result);
	return $result;	
}

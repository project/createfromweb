<?php

function createfromweb_operator_bibtexpaste_name() {
	return "Paste BibTex entry";
}

function createfromweb_operator_bibtexpaste_execute($rawtext) {
	return bibtexpaste_bibtex_parse($rawtext);
}


function bibtexpaste_bibtex_parse($rawtext) {
#	$path = drupal_get_path('module', 'easycreate');
	require_once(dirname(__FILE__) . '/classes/BibTex.php');

	$foo = new Structures_BibTex();
	$foo->content = $rawtext;
	$foo->parse();

	foreach($foo->data as $entry) {

		/* need mapping of bibtex entry items/lines to content type attributes */

		#print $entry['title']."\n";
		#print_r($entry['author']);
		#_easycreate_compileNode($entry);
		return bibtexpaste_bibtex_to_flatarray($entry);
		#break; //only first bibtex entry
	}

}

function bibtexpaste_bibtex_to_flatarray($entry) {
	$flat = array();
	
	#print $entry['url'];
	#print_r($entry);
	
	$flat['title'] = $entry['title'];
	#$flat['authors'] = implode('; ', $entry['author']);
	#$flat['authors'] = implode(' ', array_values($entry['author']));
	#$flat['authors'] = implode('; ', $multauths); # BibTeX.php now sep's too much 4me:)
	
	foreach($entry['author'] as $oneauth) {
		$multauths[] = str_replace('  ', ' ', trim(implode(' ', array_values($oneauth))));
	}
	$flat['authors'] = implode('; ', $multauths);	
	
	$flat['url'] = _firstnotnull($entry['url'], $entry['ee']);
	$flat['abstract'] = $entry['abstract'];
	$flat['year'] = $entry['year'];
	$flat['doi'] = $entry['doi'];
	  $temp_journal = $entry['volume'] ? ", ".$entry['volume'] : "";
	  $temp_journal.= _firstnotnull($entry['priority'], $entry['issue']) ? " ("._firstnotnull($entry['priority'], $entry['issue']).")" : "";
	$flat['reference'] = _firstnotnull($entry['booktitle'], $entry['journal'])
	  . $temp_journal	  
	  . ", ".$entry['year'];
	
	//experimental: get google citations upon creating new node
	if ($flat['title'] && $flat['title']!='') {
		$flat['citations'] = transformer_operator_gscholarcitations_execute($flat['title'], $flat['authors']);
	}
	
	return $flat;
}
?>
<?php

//test:
#$test = "123abc456def789ghi";
#print_r(getNextEntryTail($test,"abc", "789"));

#print _gs_getContent("http://scholar.google.com/scholar?hl=en&lr=&cites=11767853031465773370");
#_gs_getContent("11767853031465773370");

#print _gs_getContent("http://scholar.google.com/scholar?q=data+cleaning");


#$c = file_get_contents('scholar.htm');
#print $c;
#$entries = _gs_parse_page($c);
#foreach($entries as $entry) {
#	print $entry;
#	print_r(_gs_parse_entry($entry));
#}

#print_r(_gs_parse_entry($entries[0]));


function createfromweb_operator_googlescholar_name() {
	return "GoogleScholar scraping";
}

function createfromweb_operator_googlescholar_execute($title) {
	#get url, #get bibtex
	#$bibtex = bibtex_from_google($title);
	#get flat
	#return bibtex_parse($bibtex); //from operator citeulike ?
	$entries = _gs_parse_page(_gs_getContent($title));
	#return _gs_parse_entry($entries[0]);  //return first hit only
	
	#print "entries: ".count($entries);
	
	$results = array(); // !!! ?
	foreach($entries as $entry) {
		$results[] = _gs_parse_entry($entry);
	}
	#print_r($results);
	return $results;
	#return gs_scraping($title);
	
}
/**
 * @param $content
 *   haystack
 * @param $str_start
 *   start string
 * @param $str_end 
 *   end string
 * @param $switch
 *   0=exclude both sides (default); 1=include left only; 2=include right only; 3=include both
 * @return 
 *   array with string between start/end (entry) and tail (match)
 * 
 */
function getNextEntryTail($content, $str_start, $str_end, $switch = 0) {
	$offset_start = 0;
	$offset_end = 0;
	switch ($switch) {
		case 0: //excluding_both
			$offset_start = strlen($str_start);
			break;
		case 1: //including_left,excluding_right
			break;
		case 2: //excluding_left,including_right
			$offset_start = strlen($str_start);
			$offset_end   = strlen($str_end);
			break;
		case 3: //including_both
		default: //including both
			$offset_end   = strlen($str_end);
	}
	
	$result = array(); // 0: entry, 1: tail
	$index_start     = strpos($content, $str_start) + $offset_start;
	$str_entry_tail  = substr($content, $index_start); 
	$index_end       = strpos($str_entry_tail, $str_end) + $offset_end;
	$str_entry       = substr($str_entry_tail, 0, $index_end);
	$str_tail        = substr($str_entry_tail, $index_end);
	
	$result[] = $str_entry;
	$result[] = $str_tail;
	if ($str_entry != '')
		return $result;
	else return false;
}

function _gs_getContent($q) {
	$gsbase = 'http://scholar.google.com/scholar?';
	if (substr($q,0,strlen($gsbase)) != $gsbase) {
		$queryencoded = urlencode($q); #urlencode('intitle:"'.$title.'"');
		$gsurl = "http://scholar.google.com/scholar?q=TITLE&btnG=Search";
		$url = preg_replace("/TITLE/", $queryencoded, $gsurl);
	} else { 
		// given parameter is a GS-url
		$url = $q;
	}
	#print $url;
	#die($url);
	$content = file_get_contents_cache($url);
	return $content;
}

function file_get_contents_cache($url) {
	$path = file_directory_temp()."/";
	#$path = drupal_get_path('module', 'createfromweb')."/";
	$filename = $path."gscache/" . preg_replace("/[^A-Za-z0-9_\.]/", "_", $url);
	if (file_exists($filename))
		return file_get_contents($filename);
	//else:
	$contents = file_get_contents($url);
	file_put_contents($filename, $contents);
	global $user;
	if ($user->uid>0) {
		drupal_set_message("retrieved ".l('url', $url));
	}
	return $contents;
}

function _gs_parse_page($content) {
	
	$result = array();
	$more = true;
	while ($more) {
		#$cuts = getNextEntryTail($content, '<p class=g><span class="w"><a', "</font></a></font>  </font>  ", 1); #not working 2007-01
		$cuts = getNextEntryTail($content, '<p class=g><span class="w"><a', "</font>   ", 1);
		if ($cuts) {
			$result[] = $cuts[0];
			$content = $cuts[1];
		} else $more=false;
	}
	return $result;
	#print_r($result);
		
	/*
	$entrystartindex = strpos($content, '<p class=g><span class="w"><a'); #<span class="w"><a href="
	#$entryendindex = strpos($content, "Web Search</font"); # <font color=#7777CC>Web Search</font></a></font>  </font>  
	$entryendindex = strpos($content, "</font></a></font>  </font>  "); # <font color=#7777CC>Web Search</font></a></font>  </font>
	$firstentry = substr($content, $entrystartindex, $entryendindex-$entrystartindex);
	*/
}

function _gs_parse_entry($entry) {	
	
	/* */
	$entry = str_replace('<b>','',$entry);
	$entry = str_replace('</b>','',$entry);
	
	#print $entry;
	
	#$pattern = 'q=([^"]+)">([^<]+)<';  # <b possible in title! remove firstofall
	$pattern = '<p class=g><span class="w"><a href="(.+q=)?(http[^"]+)"[^>]*>([^<]+)</a>';
	$matches = array();
	preg_match("|".$pattern."|", $entry, $matches); #first only?  _all
	$url = $matches[2];
	$title = $matches[3];
	
	#print_r($matches);
	
	#$pattern = '<font color=green>(.*?)</font>';
	$pattern = '<span class="a">(.*?)</span>';
	preg_match("|".$pattern."|", $entry, $matches); #first only?  _all
	$authorvenueyear = $matches[1];
	
	$authorvenueyear = str_replace(' &hellip;', '', $authorvenueyear);
	
	#print_r($matches);
	
	$pattern = '([^-]+) - ([^,]*)([,-] ([1-2][0-9][0-9][0-9]))?';
	preg_match("|".$pattern."|", $authorvenueyear, $matches);
	#F Giunchiglia, M Yatskevich, E Giunchiglia - Proceedings of ESWC, 2005 - Springer 
	#M Yatskevich, F Giunchiglia, P Avesani - 2006 - eprints.biblio.unitn.it 
	#PA Bernstein, S Melnik, M Petropoulos, C Quix - ACM SIGMOD Record, 2004 - portal.acm.org 
	
	#print_r($matches);
	
	$authors = $matches[1];
	$venue = $matches[2];
	$year = $matches[4];
	
	##$authors = preg_replace('/([A-Z])([A-Z]| )/', '$1.', $authors);
	
	preg_match('| href="/scholar\?([^"]*cites=([0-9]+))">|', $entry, $matches);
	#  <a href="/scholar?hl=en&lr=&safe=off&cites=11767853031465773370">
	#$citedlink = $matches[0]; // plain relative html-link
	if ($matches[1])
		$citedlink = 'http://scholar.google.com/scholar?' . $matches[1]; // absolute
	// add num=100 ?!

	preg_match('| href="/scholar\?[^"]*q=cache:([^\+]+)[^"]*">View as HTML</a>|', $entry, $matches);
	#  <a class=fl href="/scholar?hl=en&lr=&safe=off&q=cache:RQ_htW7rYiwJ:cuttlefish.anu.edu.au/publications/2003/kdd03-6pages.pdf+comparison+of+fast+blocking+methods+for+record+linkage">View as HTML</a>
	#$citedlink = $matches[0]; // plain relative html-link
	if ($matches[1])
		$viewhtmllink = 'http://scholar.google.com/scholar?q=cache:' . $matches[1]; // absolute

	
	// excerpt:
	// </span><br>   ---  <b>...</b> <br>
	// <b>'s filtered out above!?
	$entryline = preg_replace("/\r|\n/","",$entry);
	#print $entryline;
	preg_match('|</span><br>(.*) ... <br>|', $entryline, $matches);
	$excerpt = $matches[1];
	$excerpt = preg_replace("/<br>/"," ",$excerpt);
	#print_r($matches);
	#print $excerpt;

	
	
	preg_match("/>Cited by ([0-9]+)</", $entry, $matches);
	#$citedby = 0; # Array ( [0] => >Cited by 130< [1] => 130 ) 
	$citedby = $matches[1]; 
	
	#todo: remove all html entities &hellip; and <b>stuff
	
	#lentitites
	
	#strip tags not needed anymore coz removed <b> b4hand.
	
	$flat = array();
	$flat['title'] = operator_safechars(strip_tags($title));
	$flat['authors'] = operator_safechars(strip_tags($authors));
	if (function_exists('transformer_operator_normalizeauthors_execute'))
		$flat['authors'] = transformer_operator_normalizeauthors_execute($flat['authors']);
	$flat['reference'] = operator_safechars(strip_tags($venue));
	$flat['year'] = strip_tags($year);
	$flat['citations'] = $citedby;
	$flat['url'] = urldecode($url);
	$flat['citedlink'] = urldecode($citedlink);
	$flat['excerpt'] = operator_safechars($excerpt);
	$flat['htmllink'] = urldecode($viewhtmllink);
	return $flat;
	
	
	## no spec. php function for InternetCombineUrl($absolute, $relative) {
}

function operator_safechars($s, $level = 0) {
#	$s = utf8_decode($s);
#	return addcslashes($s, "\0..\37!@\@\177..\377"); // addcslashes($t, "\0..\37!@\@\177..\377");

	   // replace numeric entities

   $s = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $s);
   $s = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $s);
   	   $s = utf8_encode($s);
   return $s;
	#return addcslashes($s, "\0..\37!@\@\177..\377"); // addcslashes($t, "\0..\37!@\@\177..\377");
	#return drupal_convert_to_utf8($s,'utf-8');
}
	

/*
function bibtex_from_google($title) {
	$queryencoded = urlencode('intitle:"'.$title.'"');
	$gsurl = "http://scholar.google.com/scholar?q=TITLE&btnG=Search";
	$url = preg_replace("/TITLE/", $queryencoded, $gsurl);
	#die($url);
	drupal_set_message("Querying ".l('URL', $url));

	$content = file_get_contents($url);
	$pattern = '<a class=l href="([^"]+)"';
	$matches = array();
	preg_match_all("|".$pattern."|", $content, $matches);
	
	## cookie needed to be allowed to retrieve bibtex link  
	
	return $bibtex;
}
*/

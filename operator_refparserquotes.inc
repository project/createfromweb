<?php

#$s = 'Ding Zhou, Xiang Ji, Hongyan Zha, C. Lee Giles, "Topic Evolution and Social Interactions: How Authors Effect Research," Proceedings of the 15th Conference on Information and Knowledge Management (CIKM 2006), 2006.';
#print_r(createfromweb_operator_refparserquotes_execute($s));

#$s = 'Do, H. H., E. Rahm: COMA - A System for Flexible Combination of Match Algorithms. VLDB 2002';
#$s = 'Madhavan, J. et al.: Corpus-based Schema Matching. Workshop on Information Integration on the Web (IIWeb), 2003';
#$s = 'Melnik, S., H. Garcia-Molina, E. Rahm: Similarity Flooding - A Versatile Graph Matching Algorithm. ICDE 2002';


#$a = createfromweb_operator_refparserquotes_execute($s);
#print_r($a);


function createfromweb_operator_refparserquotes_name() {
	return "Paste a textual reference";
}


#function atomizeauthors($string) {
function createfromweb_operator_refparserquotes_execute($string) {
	/* authors title venue year */
#	$r = array();

#	if (strpos($string, '"')) $pattern = '/^([^"]+)"([^"]+)"(.*)$/';
#	if (strpos($string, ":")) $pattern = '/^([^:]+):([^\.]+)\.(.*)$/';

	$p_quote = '/^([^"]+)"([^"]+)"(.*)$/';
	$p_colon = '/^([^:]+):([^\.]+)\.(.*)$/';

	$r_quote = extractor($string, $p_quote);
	$r_colon = extractor($string, $p_colon);

	#$r_diff = array_diff_assoc($r_quote, $r_colon);
	#$r_diff = array_diff_assoc($r_colon, $r_quote);

	if (count($r_quote) > count($r_colon)) {
		return $r_quote;
	} else {
		return $r_colon;
	}
}

function extractor($string, $pattern) {
	$r = array();
	$tc = " .,:\t\n\r\0\x0B"; //for trim
	if (preg_match($pattern, $string, $matches)) {
#		$r['authors'] = trim($matches[1], $tc);
#		$r['title'] = trim($matches[2], $tc);
#		$r['venue'] = trim($matches[3], $tc);
#		$r['year'] = createfromweb_operator_refparserquotes_extractyear($r['venue']);
		#if (file_exists("../transformer/transformer_normalizeauthors.inc"))
		#	include("../transformer/transformer_normalizeauthors.inc");
		$a = trim($matches[1], $tc);
		$t = trim($matches[2], $tc);
		$v = trim($matches[3], $tc);
		$y = createfromweb_operator_refparserquotes_extractyear($v);

		if (strlen($a)>0) $r['authors'] = $a;
		if (strlen($t)>0) $r['title']   = $t;
		if (strlen($v)>0) $r['venue']   = $v;
		if (strlen($y)>0) $r['year']    = $y;


		if (function_exists('transformer_operator_normalizeauthors_execute'))
			$r['authors'] = transformer_operator_normalizeauthors_execute($r['authors']);
	}
	return $r;
}

function createfromweb_operator_refparserquotes_extractyear($v) {
	preg_match('/((19|20)[0-9][0-9])/', $v, $matches);
	#print_r($matches);
	return $matches[1];
}

?>
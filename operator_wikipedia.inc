<?php


function createfromweb_operator_wikipedia_name() {
	return "some content from wikipedia article";
}

function createfromweb_operator_wikipedia_execute($query) {
	$result = array();
	
	unset($_SESSION['createfromweb']['operator']);
	
	$gurl = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=";
	$gurl.= urlencode('site:wikipedia.org/wiki/ '.$query); // intitle, quotes, ... ?
	$gjson = file_get_contents($gurl);
	$_SESSION['createfromweb']['operator']['result1'] = $gjson;
	$gres = json_decode($gjson);
	$url = (string)$gres->responseData->results[0]->unescapedUrl; // get first google match
	$rawurl = str_replace(".wikipedia.org/wiki/", ".wikipedia.org/w/index.php?title=", $url);
	$rawurl.= "&action=raw&section=0";
	
	$title = trim(preg_replace("#.*?wikipedia.org/wiki/|_#", " ", $url));
	
	drupal_set_message("getting: ".$url); // " -> ".$rawurl);

	$raw = (file_get_contents($rawurl)) or drupal_set_message("could not retrieve wikidata", ERROR);

	#$_SESSION['createfromweb']['operator']['result2'] = $raw;
		
	$raw = preg_replace("|<!--(.*?)-->|","",$raw); // no html comments
	$raw = preg_replace("/\[\[(.*?)(\|(.*?))?\]\]/", "$1", $raw); // no wiki/alias links
	$raw = preg_replace("/\[(.*?) (.*?)\]/", "$1", $raw); // only http urls

	preg_match_all('/\{((?>[^{}]+)|(?R))*\}/x', $raw, $boxes);
	foreach($boxes[1] as $box) {
		if (preg_match('/\{Infobox/', $box)) {
			$infobox = $box;
			break;
		}
	}

	#$_SESSION['createfromweb']['operator']['infobox'] = $infobox;
	$body = preg_replace('/{{.*?}}/ms','',$raw); //rest w/out boxes
	$body = preg_replace("/'''([^']*?)'''/", "<strong>$1</strong>", $body);	
	$body = preg_replace("/''([^']*?)''/", "<em>$1</em>", $body);
	$infobox = preg_replace("|<br ?/?>|","; ", $infobox);  // html linebreaks to semicolons
	$infobox = strip_tags($infobox);	
	$result = getBoxProperties($infobox);
	$result['title'] = $title;
	$result['body'] = trim($body);
	
	$_SESSION['createfromweb']['operator']['result'] = $result;	
	//$result = array($result);
	return $result;
}

    /**
     * http://code.google.com/p/linuxpedia/
     * Retrieves properties defined in an infobox as an associative array
     *
     * @param   $box    Infobox code
     * @param   $toLower    Whether to convert all predicate keys to lowercase
     * @return  Associative array with predicates as keys
     */
    function getBoxProperties($box, $toLower = false) {

        /* Remove outside curly brackets */
        $box = substr($box, 1, strlen($box) - 2);

        /* Remove HTML comments */
        $box = preg_replace('/<\!--[^>]*->/mU', '', $box);

        /* Split triples; ignoring triples in subtemplates */
        $triples = preg_split('/\| (?! [^{]*\}\} | [^[]*\]\] )/x',$box);

        $a = array();

        foreach ($triples as $triple) {
                $predObj = split('=',$triple,2);

                if (count($predObj) == 2 && ($pred = trim($predObj[0])) != "" && ($obj = trim($predObj[1])) != "")
                {
                    $key = ($toLower ? strtolower($pred) : $pred);
                    $a[$key] = $obj;
                }
        }

        return $a;
    }

?>
